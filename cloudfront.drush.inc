<?php // $Id$

/**
 * @file Cloudfront drush command.
 */

drush_generate_include_cloudfront();

/**
 * Implementation of hook_drush_command()
 */
function cloudfront_drush_command() {
  $items = array();

  $items['cloudfront-process'] = array(
    'callback' => 'cloudfront_drush_processqueue',
    'description' => dt('Process the Cloudfront queue'),
  );

  return $items;
}


/**
 * Implementation of hook_drush_help().
 */
function cloudfront_drush_help($section) {
  switch ($section) {
    case 'drush:cloudfront-process':
      return dt('Process the Cloudfront queue.');
  }
}


/**
 * Callback function
 */
function cloudfront_drush_processqueue() {

  if (drush_confirm(dt("Are you sure you want to process queue?"))) {
    _cloudfront_process_queue();
    drush_print(dt("Finished processing queue."));
  }
}


// include cloudfront.module
function drush_generate_include_cloudfront() {
  require_once('cloudfront.module');
}
